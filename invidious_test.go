package invidious

import (
	"context"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetVideo_Success(t *testing.T) {
	videoID := "sO0DKamA1v4"

	handler := http.NewServeMux()
	handler.HandleFunc("/api/v1/videos/"+videoID, func(w http.ResponseWriter, r *http.Request) {
		data, _ := ioutil.ReadFile("./test_data/get_video_success.json")
		_, _ = w.Write(data)
	})

	srv := httptest.NewServer(handler)
	defer srv.Close()

	client := NewClient(srv.URL)

	video, err := client.GetVideo(context.Background(), videoID)
	require.NoError(t, err)
	assert.Equal(t, videoID, video.VideoID)
	assert.Equal(t, 678, video.LengthSeconds)
}
