package invidious

type Video struct {
	Type            string `json:"type"`
	Title           string `json:"title"`
	VideoID         string `json:"videoId"`
	VideoThumbnails []struct {
		Quality string `json:"quality"`
		URL     string `json:"url"`
		Width   int    `json:"width"`
		Height  int    `json:"height"`
	} `json:"videoThumbnails"`
	Storyboards []struct {
		URL              string `json:"url"`
		TemplateURL      string `json:"templateUrl"`
		Width            int    `json:"width"`
		Height           int    `json:"height"`
		Count            int    `json:"count"`
		Interval         int    `json:"interval"`
		StoryboardWidth  int    `json:"storyboardWidth"`
		StoryboardHeight int    `json:"storyboardHeight"`
		StoryboardCount  int    `json:"storyboardCount"`
	} `json:"storyboards"`
	Description      string      `json:"description"`
	DescriptionHTML  string      `json:"descriptionHtml"`
	Published        int         `json:"published"`
	PublishedText    string      `json:"publishedText"`
	Keywords         []string    `json:"keywords"`
	ViewCount        int         `json:"viewCount"`
	LikeCount        int         `json:"likeCount"`
	DislikeCount     int         `json:"dislikeCount"`
	Paid             bool        `json:"paid"`
	Premium          bool        `json:"premium"`
	IsFamilyFriendly bool        `json:"isFamilyFriendly"`
	AllowedRegions   []string    `json:"allowedRegions"`
	Genre            string      `json:"genre"`
	GenreURL         interface{} `json:"genreUrl"`
	Author           string      `json:"author"`
	AuthorID         string      `json:"authorId"`
	AuthorURL        string      `json:"authorUrl"`
	AuthorThumbnails []struct {
		URL    string `json:"url"`
		Width  int    `json:"width"`
		Height int    `json:"height"`
	} `json:"authorThumbnails"`
	SubCountText    string  `json:"subCountText"`
	LengthSeconds   int     `json:"lengthSeconds"`
	AllowRatings    bool    `json:"allowRatings"`
	Rating          float64 `json:"rating"`
	IsListed        bool    `json:"isListed"`
	LiveNow         bool    `json:"liveNow"`
	IsUpcoming      bool    `json:"isUpcoming"`
	DashURL         string  `json:"dashUrl"`
	AdaptiveFormats []struct {
		Index          string `json:"index"`
		Bitrate        string `json:"bitrate"`
		Init           string `json:"init"`
		URL            string `json:"url"`
		Itag           string `json:"itag"`
		Type           string `json:"type"`
		Clen           string `json:"clen"`
		Lmt            string `json:"lmt"`
		ProjectionType string `json:"projectionType"`
		Fps            int    `json:"fps"`
		Container      string `json:"container"`
		Encoding       string `json:"encoding"`
		Resolution     string `json:"resolution,omitempty"`
		QualityLabel   string `json:"qualityLabel,omitempty"`
	} `json:"adaptiveFormats"`
	FormatStreams []struct {
		URL          string `json:"url"`
		Itag         string `json:"itag"`
		Type         string `json:"type"`
		Quality      string `json:"quality"`
		Fps          int    `json:"fps"`
		Container    string `json:"container"`
		Encoding     string `json:"encoding"`
		Resolution   string `json:"resolution"`
		QualityLabel string `json:"qualityLabel"`
		Size         string `json:"size"`
	} `json:"formatStreams"`
	Captions []struct {
		Label        string `json:"label"`
		LanguageCode string `json:"languageCode"`
		URL          string `json:"url"`
	} `json:"captions"`
	RecommendedVideos []struct {
		VideoID         string `json:"videoId"`
		Title           string `json:"title"`
		VideoThumbnails []struct {
			Quality string `json:"quality"`
			URL     string `json:"url"`
			Width   int    `json:"width"`
			Height  int    `json:"height"`
		} `json:"videoThumbnails"`
		Author        string `json:"author"`
		AuthorURL     string `json:"authorUrl"`
		AuthorID      string `json:"authorId"`
		LengthSeconds int    `json:"lengthSeconds"`
		ViewCountText string `json:"viewCountText"`
		ViewCount     int    `json:"viewCount"`
	} `json:"recommendedVideos"`
}
