package invidious

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

const apiPrefix = "/api/v1"

type Client struct {
	hostname string
	client   *http.Client
}

func NewClient(hostname string) *Client {
	httpClient := &http.Client{}
	return &Client{
		hostname: hostname,
		client:   httpClient,
	}
}

var ErrVideoNotFound = errors.New("invidious video not found")

func (c *Client) GetVideo(ctx context.Context, id string) (Video, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, c.hostname+apiPrefix+"/videos/"+id, nil)
	if err != nil {
		return Video{}, fmt.Errorf("failed to construct invidiousd api request: %v", err)
	}

	res, err := c.client.Do(req)
	if err != nil {
		return Video{}, fmt.Errorf("failed to send invidious api request %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusNotFound {
		return Video{}, ErrVideoNotFound
	}
	if res.StatusCode != http.StatusOK {
		return Video{}, fmt.Errorf("unexpected error while getting invidious video: %v", err)
	}

	var video Video
	if err := json.NewDecoder(res.Body).Decode(&video); err != nil {
		return Video{}, fmt.Errorf("failed to decode invidious response: %v", err)
	}

	return video, nil
}
